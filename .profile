#!/bin/sh

source /etc/profile

# Variables
export EDITOR="emacsclient -nw"
export TERM="rxvt-unicode-256color"
export BROWSER="firefox"
export XDG_CONFIG_HOME="$HOME/.config"
export ZSH_CONFIG="$XDG_CONFIG_HOME/zsh"

# Path
export GOROOT=/usr/lib/go            
export GOPATH=$HOME/.go
PATH+=$HOME/.local/bin:
PATH+=$HOME/.bin:
PATH+=$HOME/.cargo/bin:
PATH+=$GOROOT/bin

# IBus
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
