export PS1="%{%F{8}%}[%{%F{1}%}%n%{%F{8}%}@%{%F{5}%}%m%{%F{8}%}] %{%F{8}%}[%{%F{4}%}%~%{%F{8}%}] %{%F{2}%}-> %{%f%}"

# Emacs tramp fix
if [[ "$TERM" == "dumb" ]]
then
  unsetopt zle
  unsetopt prompt_cr
  unsetopt prompt_subst
  unfunction precmd
  unfunction preexec
  PS1='$ '
fi
