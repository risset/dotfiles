# Remove all files in cwd except arg
rmexcept()
{
    find . -maxdepth 1 ! -name $1 -type f -exec rm {} +
}

# Show the number of files deeper than the current dir
numfiles() 
{
    find . -mindepth 1 -type f | wc -l
}

# Make a bootable USB drive from a Linux ISO
mkdrive()
{
    if [[ $# -ne 2  ]]; then
	echo 'mkdrive <path/to/iso> <disk>' 
    else
	doas dd if=$1 of=$2 bs=4M status=progress oflag=sync
    fi
}

# Upload file to ix.io pastebin service and copy URL to clipboard
pb()
{
    cat $1 |& curl -F 'f:1=<-' ix.io | xclip -sel clip
}

# List aws instances
aws-list-instances()
{
    local filters="Name=instance-type,Values=t2.micro"
    local query="Reservations[].Instances[].InstanceId"
    aws ec2 describe-instances --filters $filters --query $query
}

# Add torrent to transmission-daemon
tmr-add()
{
    transmission-remote --add "$1"
}

# cd to directory via fzf
fd()
{
    local dir=$(find ${1:-.} -type d 2> /dev/null | fzf +m)
    cd "$dir"
}

# cd to directory via fzf (anywhere on filesystem via mlocate)
cf()
{
  local file="$(locate -Ai -0 $@ | grep -z -vE '~$' | fzf --read0 -0 -1)"

  if [[ -n $file ]]; then
     if [[ -d $file ]]; then
        cd -- $file
     else
        cd -- ${file:h}
     fi
  fi
}
