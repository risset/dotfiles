export EDITOR="emacsclient -nw"
export BROWSER="firefox"
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
export GOROOT=/usr/lib/go            
export GOPATH=$HOME/.go
