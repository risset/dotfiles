# Load initial environment
source $HOME/.profile

# Load configuration files
for f in $(find $ZSH_CONFIG/)
do
    source $f
done
