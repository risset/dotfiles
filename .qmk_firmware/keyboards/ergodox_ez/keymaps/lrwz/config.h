#ifndef ERGODOX_LRWZ_CONFIG_H
#define ERGODOX_LRWZ_CONFIG_H

// Glow colours
#define RGBLIGHT_COLOR_LAYER_0 0xB0, 0x40, 0xF0

// Tapping term
// #undef TAPPING_TERM
// #define TAPPING_TERM 200

// Extra MT keycodes
// #define LCTL_T(kc) MT(MOD_LCTL, kc)
// #define RCTL_T(kc) MT(MOD_RCTL, kc)

#endif /* ERGODOX_LRWZ_CONFIG_H */
