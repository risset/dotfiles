module Layouts where

import qualified Config as C

import XMonad
import XMonad.Hooks.ManageDocks
import XMonad.Layout.ResizableTile
import XMonad.Layout.Renamed
import XMonad.Layout.PerWorkspace
import XMonad.Layout.NoBorders
import XMonad.Layout.MultiColumns

layoutHook = onWorkspace "music" mirrorTall
           $ onWorkspace "supercollider" columns
           $ onWorkspace "haskell" columns
           $ onWorkspace "go" columns
           $ onWorkspace "blog" columns
           $ onWorkspace "ricing" columns
           $ onWorkspace "video" full
           $ onWorkspace "game" full
           $ tall
  where
    full = useIcon "full"
         $ noBorders Full

    tall = useIcon "tall"
         $ avoidStruts
         $ smartBorders
         $ ResizableTall 1 (1/100) (50/100) []


    mirrorTall = useIcon "tall_bottom"
               $ avoidStruts
               $ smartBorders
               $ Mirror
               $ ResizableTall 1 (1/100) (70/100) []

    columns = useIcon "tall"
            $ avoidStruts
            $ multiCol [1] 1 0 (-0.3275)

    useIcon name = renamed [Replace $ C.icon name]
