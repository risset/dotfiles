module Config where

import XMonad
import Graphics.X11.Types 
import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch

black :: [Char]
black = "#000000"

white :: [Char]
white = "#b5b5b5"

red :: [Char]
red = "#f92672"

green :: [Char]
green = "#a6e22e"

yellow :: [Char]
yellow = "#f4bf75"

blue :: [Char]
blue = "#66d9ef"

magenta :: [Char]
magenta = "#ae81ff"

cyan :: [Char]
cyan = "#a1efe4"

gray :: [Char]
gray = "#75715e"

modMask :: Graphics.X11.Types.KeyMask
modMask = mod4Mask  

term :: [Char]
term = "urxvtc"

emacs :: [Char]
emacs = term ++ " -e emacsclient -s /tmp/emacs1000/server -nw"

browser :: [Char]
browser = "firefox"

icon :: [Char] -> [Char]
icon name = "<icon=/home/lrwz/.icons/" ++ name ++ ".xbm/>"

promptTheme :: XPConfig
promptTheme = def { searchPredicate = fuzzyMatch
                  , alwaysHighlight = True
                  , sorter = fuzzySort
                  , autoComplete = Just 100000
                  , borderColor = white
                  , bgHLight = black
                  , fgHLight = red
                  , bgColor = black
                  , fgColor = white
                  , font = "xft:xos4 Terminus:pixelsize=10"
                  , position = CenteredAt 0.45 0.5
                  , promptBorderWidth = 1
                  , height = 24
               }
