module Bar where

import qualified Config as C

import XMonad.Hooks.DynamicLog            
import XMonad.Hooks.DynamicBars
import XMonad.Util.Run
import XMonad.Util.NamedScratchpad

logPP = namedScratchpadFilterOutWorkspacePP def
  { ppCurrent = xmobarColor C.red "" . pad
  , ppVisible = xmobarColor C.white "" . pad
  , ppHiddenNoWindows = xmobarColor C.gray "" . pad
  , ppTitle   = xmobarColor C.green "" . shorten 75 . pad
  , ppHidden  = wrap " " " "
  , ppSep     = pad $ xmobarColor C.gray "" " "
  , ppLayout  = xmobarColor C.magenta "" . pad
  }

logPPActive :: PP
logPPActive = logPP
  { ppCurrent = xmobarColor C.red "" . pad
  }

barOne = "xmobar ~/.xmobar/screen1.conf -x 0"
barTwo = "xmobar ~/.xmobar/screen2.conf -x 1"
barCreator s = spawnPipe $ if s == 0 then barOne else barTwo
logHook = multiPP logPPActive logPP
