module Keys where

import qualified Config as C

import XMonad 
import qualified XMonad.StackSet as W   
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicProjects
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.GridSelect
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Prompt.Pass
import XMonad.Prompt.Shell
import XMonad.Prompt.Window
import XMonad.Prompt.DirExec
import XMonad.Util.Run
import XMonad.Util.Scratchpad
import XMonad.Util.WorkspaceCompare

keys :: [([Char], X())]
keys = 
    -- Prompts
    [ ("M-r", spawn "rofi -show run")
    , ("M-p", spawn "rofi-pass")
    , ("M-o", spawn "rofi-open")
    , ("M-w", windowPrompt C.promptTheme Goto allWindows)
    , ("M-g", switchProjectPrompt C.promptTheme)

   -- Programs 
    , ("M-<Return>", spawn C.term)
    , ("M-<Space>", scratchpadSpawnActionTerminal C.term)

    -- -- Media
    , ("M--", spawn "maim-clip")
    , ("M-S--", spawn "maim-clip -s")
    , ("M-=", spawn "mpc toggle")
    , ("M-`", spawn "switch-input-method")

    -- -- Window management
    , ("M-<Backspace>", kill)

    -- -- Workspaces & Screens
    , ("M-a", toggleWSNoNSP)
    , ("M-s", nextScreen)
    , ("M-l", changeWS Next)
    , ("M-h", changeWS Prev)
    , ("M-C-s", swapNextScreen)
    , ("M-S-<Backspace>", removeWorkspace)
    ]
  where
    -- Switch to last workspace, ignoring scratchpad
     toggleWSNoNSP = toggleWS' ["NSP"]

     -- Change workspace left or right, ignoring scratchpad
     getSortByIndexNoNSP = fmap (.scratchpadFilterOutWorkspace) getSortByIndex
     changeWS dir = windows . W.greedyView =<<
       findWorkspace getSortByIndexNoNSP dir HiddenNonEmptyWS 1
