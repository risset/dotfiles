module Startup where

import qualified Bar as B

import XMonad
import XMonad.Hooks.SetWMName
import XMonad.Hooks.DynamicBars

barDestroyer = return ()
startupHook = do
    setWMName "XMonad"
    dynStatusBarStartup B.barCreator barDestroyer
    spawn "xrdb -merge ~/.Xresources" -- Reload X resources
    spawn "~/.fehbg" -- Reload wallpapers
