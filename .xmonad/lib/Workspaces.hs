module Workspaces where

import qualified Config as C

import XMonad
import XMonad.Actions.DynamicProjects

spaces :: [[Char]]
spaces = ["admin", "music", "chat", "web"]

projects :: [Project]
projects =
    [ Project { projectName      = "admin"
              , projectDirectory = "/etc/portage"
              , projectStartHook = Just $ do spawn C.term
                                             spawn C.term
                                             spawn C.term
              }

    , Project { projectName      = "music"
              , projectDirectory = "/mnt/data/music"
              , projectStartHook = Just $ do spawn "urxvtc -e ncmpcpp"
                                             spawn "urxvtc -e cava"
              }

    , Project { projectName      = "chat"
              , projectDirectory = "~"
              , projectStartHook = Just $ do spawn "apulse discord"
              }

    , Project { projectName      = "web"
              , projectDirectory = "~/dl/http"
              , projectStartHook = Just $ do spawn "firefox"
              }

    , Project { projectName      = "org"
              , projectDirectory = "~/org"
              , projectStartHook = Just $ do spawn C.term
                                             spawn C.term
                                             spawn C.term
              }


    , Project { projectName      = "blog"
              , projectDirectory = "~/blog"
              , projectStartHook = Just $ do spawn C.emacs
                                             spawn C.emacs
                                             spawn C.emacs
              }

    , Project { projectName      = "supercollider"
              , projectDirectory = "~/audio/supercollider"
              , projectStartHook = Just $ do spawn C.emacs
                                             spawn C.emacs
                                             spawn C.emacs
              }

    , Project { projectName      = "haskell"
              , projectDirectory = "~/projects/haskell/"
              , projectStartHook = Just $ do spawn C.emacs
                                             spawn C.emacs
                                             spawn C.emacs
              }

    , Project { projectName      = "go"
              , projectDirectory = "~/projects/go/"
              , projectStartHook = Just $ do spawn C.emacs
                                             spawn C.emacs
                                             spawn C.emacs
              }

    , Project { projectName      = "ricing"
              , projectDirectory = "~/.dotfiles"
              , projectStartHook = Just $ do spawn C.emacs
                                             spawn C.emacs
                                             spawn C.emacs
              }

    , Project { projectName      = "japanese"
              , projectDirectory = "~/org"
              , projectStartHook = Just $ do spawn "anki"
                                             spawn C.emacs
              }

    , Project { projectName      = "video"
              , projectDirectory = "/mnt/data/video"
              , projectStartHook = Nothing
              }

    , Project { projectName      = "game"
              , projectDirectory = "~"
              , projectStartHook = Nothing
              }

    , Project { projectName      = "steam"
              , projectDirectory = "~"
              , projectStartHook = Just $ do spawn "steam"
              }

    , Project { projectName      = "manga"
              , projectDirectory = "/mnt/data/manga"
              , projectStartHook = Just $ do spawn "zathura"
              }

    , Project { projectName      = "irc"
              , projectDirectory = "~"
              , projectStartHook = Just $ do spawn C.term
              }
    
    , Project { projectName      = "aws"
              , projectDirectory = "~"
              , projectStartHook = Just $ do spawn C.term
              }
    ]
